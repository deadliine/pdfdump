package com.pdfdump;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;

import com.pdfdump.services.PasswordService;

@SpringBootApplication
@ImportResource("classpath:mongoConfig.xml")
public class PdfDumpApplication {
    
    @Autowired
    PasswordService passwordService;
    
	public static void main(String[] args) {
		SpringApplication.run(PdfDumpApplication.class, args);
	}
	
	@Bean
    CommandLineRunner init() {
        return (args) -> {
            passwordService.addPassword("test", "test");
        };
    }
}
