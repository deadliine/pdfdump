package com.pdfdump.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.google.gson.JsonObject;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsCriteria;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.mongodb.gridfs.GridFSDBFile;
import com.pdfdump.domain.Fichier;
import com.pdfdump.repositories.FichierRepository;
import com.pdfdump.utils.Utils;

@org.springframework.stereotype.Service
public class FichierService extends Service {

    @Autowired
    FichierRepository fichierRepository;

    private final GridFsTemplate gridFsTemplate;

    @Autowired
    public FichierService(GridFsTemplate gridFsTemplate) {
        this.gridFsTemplate = gridFsTemplate;
    }

    private Logger logger = LoggerFactory.getLogger(FichierService.class);

    public String getAll() {
        List<Fichier> fichiers = fichierRepository.findAll();
        List<JsonObject> json = new ArrayList<>();
        Gson gson = new Gson();
        for (Fichier fichier : fichiers) {
            JsonElement element = gson.toJsonTree(fichier);
            element.getAsJsonObject().addProperty("filename", fichier.getId().toHexString());
            json.add(element.getAsJsonObject());
        }
        return new Gson().toJson(json);
    }

    public String addFichier(String document, String auteur, MultipartFile file, String extension) {
        if (document == null || auteur == null)
            return Utils.jsonReply("ERROR", true, "MESSAGE", "Fichier not registered");
        Fichier fichier = new Fichier(document, auteur, extension, file.getSize(), file.getContentType());
        fichier = fichierRepository.save(fichier);
        String name = fichier.getId().toHexString();
        try {
            Optional<GridFSDBFile> existing = maybeLoadFile(name);
            if (existing.isPresent()) {
                gridFsTemplate.delete(getFilenameQuery(name));
            }
            gridFsTemplate.store(file.getInputStream(), name, file.getContentType()).save();
        } catch (IOException e) {
        }
        return Utils.jsonReply("ERROR", false, "MESSAGE", "Fichier registered successfully");
    }

    public String addFichier(String document, String auteur, MultipartFile file) {
        if (document == null || auteur == null)
            return Utils.jsonReply("ERROR", true, "MESSAGE", "Fichier not registered");
        Fichier fichier = new Fichier(document, auteur, getExtention(file), file.getSize(), file.getContentType());
        fichier = fichierRepository.save(fichier);
        String name = fichier.getId().toHexString();
        try {
            Optional<GridFSDBFile> existing = maybeLoadFile(name);
            if (existing.isPresent()) {
                gridFsTemplate.delete(getFilenameQuery(name));
            }
            gridFsTemplate.store(file.getInputStream(), name, file.getContentType()).save();
        } catch (IOException e) {
        }
        return Utils.jsonReply("ERROR", false, "MESSAGE", "Fichier registered successfully");
    }

    private String getExtention(MultipartFile file) {
        String[] extensionArray = file.getOriginalFilename().split("\\.");
        return "." + extensionArray[extensionArray.length - 1];
    }

    private String removeExtention(String file) {
        return file.split("\\.")[0];
    }

    public String deleteFile(String fileName) {
        if (fileName == null)
            return Utils.jsonReply("ERROR", true, "MESSAGE", "Fichier is null");
        gridFsTemplate.delete(getFilenameQuery(fileName));
        fichierRepository.delete(fichierRepository.findById(new ObjectId(removeExtention(fileName))));
        return Utils.jsonReply("ERROR", false, "MESSAGE", "Fichier est supprimer");
    }

    public Optional<GridFSDBFile> maybeLoadFile(String name) {
        GridFSDBFile file = gridFsTemplate.findOne(getFilenameQuery(name));
        return Optional.ofNullable(file);
    }

    public static Query getFilenameQuery(String name) {
        return Query.query(GridFsCriteria.whereFilename().is(name));
    }

    public List<GridFSDBFile> getFiles() {
        return gridFsTemplate.find(null);
    }

    public Fichier getFichierByHex(String hex) {
        return fichierRepository.findById(new ObjectId(hex));

    }
}
