package com.pdfdump.repositories;

import com.pdfdump.domain.Fichier;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FichierRepository extends MongoRepository<Fichier, ObjectId>{

    public List<Fichier> findAll();
    public Fichier findById(ObjectId id);
    void delete(Fichier entity);
}
