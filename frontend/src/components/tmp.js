const info = new FormData();
      info.append('nom', values.titre);
      info.append('file', new Blob(values.file, { type: "audio/mp3" }));
      const request = axios({
        method: 'post',
        url:`${ROOT_URL}/file/`,
        data: info,
        config: { headers: {'Content-Type': 'multipart/form-data' }}
      });
      const data = request.then((response) => {
        const {data} = response;
        if('Televersemet Reussi' == data){
          swal(
            'Success!',
            'Votre compte a été modifié.',
            'success'
          ).then(() => {
            this.props.history.push('/accueil');
          });
        }
      })