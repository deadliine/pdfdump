import React, {Component} from 'react';
import Header from './sub_components/Header';
import { Link, withRouter} from 'react-router-dom';
import Dropzone from 'react-dropzone';
import swal from 'sweetalert2'
import axios from 'axios';
import { ROOT_URL } from '../actions';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';

class Insertion extends Component {
	renderDropzoneInput(field){
	    const files = field.input.value;
	    const test = {};
	    test.name="file";
	    test.required=true;
	    return (
	      <div>
				<br />
	        <Dropzone
	          name="file"
	          accept=""
	          multiple={false}
	          inputProps={test}
	          onDrop={( filesToUpload, e ) => field.input.onChange(filesToUpload)}
	        >
	        <div>Déposez un fichier ici laissant tomber ou cliquez sur pour sélectionner les fichiers à télécharger.</div>
	        </Dropzone>
	        {field.meta.touched &&
	          field.meta.error &&
	          <span className="error">{field.meta.error}</span>}
	        {files && Array.isArray(files) && (
	          <ul>
	            { files.map((file, i) => <li key={i}>{file.name}</li>) }
	          </ul>
	        )}
	      </div>
	    );
	  }

	  renderField(field){
	    const { meta: { touched, error }} = field;
	    return (
	      <div className="input-group">
	        <span className="input-group-addon">
	          <big>{field.label}</big>
	        </span>
	        <div className={`form-line ${error ? "style={{borderBottom: '2px solid #F44336'}}" : ''}`}>
	          <input
	            type={field.type}
	            className="form-control"
	            placeholder={field.label}
	            autoFocus={field.autofocus}
	            name={field.name}
	            style={ error ? {borderBottom: '2px solid #F44336'} : {}}
	            {...field.input}
	          />
	        </div>
	        <label id={`${field.name}-error`} className="error" htmlFor={field.name}>
	          {touched ? error : ''}
	        </label>
	      </div>
	    );
	  }

	  onSubmit(values){
	    swal({
	      title: 'Êtes-vous sûr?',
	      text: 'Est-ce que ces informations sont exactes?',
	      type: 'warning',
	      showCancelButton: true,
	      confirmButtonText: 'Oui, ajoute!',
	      cancelButtonText: 'Non, annuler'
	    }).then(() => {
				var typeFile = values.file[0].name;
				var array = typeFile.split(".");
				typeFile = "."+array[array.length-1];
				var typeMime = ""+values.file[0].type;
	      const info = new FormData();
	      info.append('document', values.nomDocument);
	      info.append('auteur', values.auteur);
	      info.append('file', new Blob(values.file, { type: typeMime }));
				info.append("extension", typeFile);
	      const request = axios({
	        method: 'post',
	        url:`${ROOT_URL}/rest`,
	        data: info,
	        config: { headers: {'Content-Type': 'multipart/form-data' }}
	      });
	      request.then((response) => {
	        const {data} = response;
	        if(data.length > 0){
	          swal(
	            'Success!',
	            'Votre document a été ajouté.',
	            'success'
	          ).then(() => {
	            this.props.history.push('/admin');
	          });
	        }
	      })
	    }, function(dismiss) {
	      // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
	      if (dismiss === 'cancel') {
	      }
	    })
	  }

	render(){
		const { handleSubmit } = this.props;
		return (
			<div>
				<Header />
				<h2>FORM</h2>
				<form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
          <div className="row">
            <div className="col-lg-8 col-md-8 col-sm-8 col-xs-8">
              <Field
                label="Titre du document"
                type="text"
                name="nomDocument"
                component={this.renderField}
              />
              <Field
                label="L'auteur du document"
                type="text"
                name="auteur"
                component={this.renderField}
              />
            </div>
	            <Field
	              name="file"
	              component={this.renderDropzoneInput}
	            />
          </div>
          <input type="hidden" name="id" /><br />
          <button className="btn btn-primary waves-effect" type="submit">Ajouter le document</button>
        </form>
				<br />
        <Link to={`/accueil`}><button className="btn btn-primary waves-effect">Retour &agrave; l&acute;accueil</button></Link>
				<Link className="btn waves-effect" to={`/admin`}><button className="btn btn-primary waves-effect">Retour &agrave; l&acute;accueil admin</button></Link>
			</div>
		);
	}
}
function mapStateToProps(state, ownProps){
  return ({
    initialValues: state.user
  });
}
function validate(values){
  const errors = {};

  return errors;
}
Insertion = reduxForm({
  validate,
  form: 'AjoutMusique'
})(Insertion);

Insertion = withRouter(connect(mapStateToProps)(Insertion));
export default Insertion;
