import React, {Component } from 'react';
import DataTables from 'material-ui-datatables';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import {fetchData, deleteData, setFetch, setNotFetch } from '../../actions';
import { connect } from 'react-redux';
import _ from 'lodash';
import Delete from 'material-ui-icons/Delete';
import { withRouter } from 'react-router-dom';
import swal from 'sweetalert2'

const default_key = 'asc';
const default_order = 'nomDocument';

class DatatableAdmin extends Component {

  componentWillReceiveProps(nextProps){
    if(nextProps.isFetch){
      this.props.fetchData();
      this.props.setNotFetch();
    }
  }

  componentDidMount(){
    this.props.fetchData();

  }

  handleSortOrderChange = (key, order) => {
    this.order = order;
    this.key = key;
    this.dataToShow = _.orderBy((this.dataToShow == null ? this.data : this.dataToShow), key,order);
    this.forceUpdate();
  }

  handleFilterValueChange = (filterValue) => {
    if(filterValue !== ''){
      this.dataToShow = [];
      this.data.forEach(d => (d.nomDocument.indexOf(filterValue) !== -1) || (d.auteur.indexOf(filterValue) !== -1) || ((d.size+'').indexOf(filterValue) !== -1) ? this.dataToShow.push(d) : null);
    }else{
      this.dataToShow = null;
    }
    this.handleSortOrderChange(this.key, this.order);
    this.forceUpdate();
  }

  deleteClick(filename){
    swal({
      title: 'Etez-vous sure?',
      text: 'Ce document sera perdu a tout jamais!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Oui, ajoute!',
      cancelButtonText: 'Non, annuler'
    }).then((value) => {
      if(value.value === true){
        deleteData(filename, () => {
            this.props.setFetch();
            swal(
              'Success!',
              'Votre document a été supprimé.',
              'success'
            ).then(() => {
              this.props.history.push('/admin');
            });
          }
        )
      }
    }, function(dismiss) {
      // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
      if (dismiss === 'cancel') {
      }
    });
  }

  render() {
    const TABLE_COLUMNS = [
      {
        key: 'nomDocument',
        label: 'Document',
        sortable: true
      }, {
        key: 'auteur',
        label: 'Auteur',
        sortable: true
      }, {
        key: 'size',
        label: 'Taille',
        sortable: true
      }, {
        key: 'filename',
        label: 'Suppression',
        render: (filename) => <FloatingActionButton onClick={(e) => this.deleteClick(filename)} mini={true} className="btn-floating" ><Delete /></FloatingActionButton>
      }
    ];
    this.data = _.values(this.props.table_data);
    var DATA = (this.dataToShow == null ? this.data : this.dataToShow);
    if(this.key === undefined || this.order === undefined){
      this.key = default_key;
      this.order = default_order;
    }
    return (
      <MuiThemeProvider>
        <DataTables
          height={'auto'}
          selectable={false}
          showRowHover={true}
          columns={TABLE_COLUMNS}
          data={DATA}
          showCheckboxes={false}
          onFilterValueChange={this.handleFilterValueChange}
          onSortOrderChange={this.handleSortOrderChange}
          showHeaderToolbar={true}
          headerToolbarMode={"filter"}
          onRowSizeChange={this.handleRowSizeChange}
          count={_.keys(DATA).length}
          filterHintText={"Recherche"}
          showFooterToolbar={false}
        />
      </MuiThemeProvider>
    );
  }
}

function mapStateToProps({ table_data, isFetch }){
  return {table_data, isFetch} ;
}

export default withRouter(connect(mapStateToProps, { fetchData, deleteData, setFetch, setNotFetch } )(DatatableAdmin));
