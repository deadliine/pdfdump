import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import promise from 'redux-promise';
import reducers from './reducers';
import SessionRoute from './containers/session_route';

const createStoreWithMiddleware = applyMiddleware(promise)(createStore);

ReactDOM.render(
		<Provider store={createStoreWithMiddleware(reducers)}>
			<BrowserRouter>
				<Switch>
				    <Route component={SessionRoute} />
				</Switch>
			</BrowserRouter>
		</Provider>
		, document.querySelector('#root')
);
